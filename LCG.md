The following packages are used from LCG102b:

- attrs
- cffi
- docopt
- exceptiongroup
- iniconfig
- install
- Jinja2
- MarkupSafe
- pluggy
- pycparser
- pytest
- tomli

